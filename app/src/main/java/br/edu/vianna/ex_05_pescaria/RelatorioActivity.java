package br.edu.vianna.ex_05_pescaria;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import br.edu.vianna.ex_05_pescaria.models.database.FakeDatabase;

public class RelatorioActivity extends AppCompatActivity {
    private TextView txtMulta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorio);

        bindings();

        calculaMulta();
    }

    private void calculaMulta() {
        double multa = FakeDatabase.pescaria.calculaMultaExcesso();
        txtMulta.setText("O valor da multa é de R$"+multa);
    }

    private void bindings() {
        txtMulta = findViewById(R.id.txtMulta);
    }
}