package br.edu.vianna.ex_05_pescaria;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import br.edu.vianna.ex_05_pescaria.models.Peixe;
import br.edu.vianna.ex_05_pescaria.models.database.FakeDatabase;

public class CadastroActivity extends AppCompatActivity {
    private Button btnSalvar;
    private TextInputEditText txtPeso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        setTitle("Cadastro");

        bindings();

        btnSalvar.setOnClickListener(callSalvar());
    }

    private View.OnClickListener callSalvar() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Peixe p = new Peixe(Integer.parseInt(txtPeso.getText().toString()));

                FakeDatabase.save(p);

                Toast.makeText(getApplicationContext(), "Salvo com sucesso!", Toast.LENGTH_LONG).show();

                finish();
            }
        };
    }

    private void bindings() {
        btnSalvar = findViewById(R.id.btnSalvar);
        txtPeso = findViewById(R.id.txtPeso);
    }
}