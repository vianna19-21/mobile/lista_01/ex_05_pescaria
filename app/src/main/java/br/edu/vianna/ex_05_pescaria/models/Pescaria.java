package br.edu.vianna.ex_05_pescaria.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Pescaria implements Serializable {
    private static final int VALOR_MULTA = 4;
    private ArrayList<Peixe> peixes;

    public Pescaria() {
        peixes = new ArrayList<Peixe>();
    }

    public Pescaria(ArrayList<Peixe> peixes) {
        this.peixes = peixes;
    }

    public ArrayList<Peixe> getPeixes() {
        return peixes;
    }

    public void addPeixe(Peixe p) {
        this.peixes.add(p);
    }

    public double calculaMultaExcesso() {
        double multa = 0;
        for (Peixe p : peixes) {
            if (p.isAcimaDoPermitido()) {
                multa += ((p.getPeso() - p.getPesoMaximo()) * VALOR_MULTA);
            };
        }

        return multa;
    }
}
