package br.edu.vianna.ex_05_pescaria.models;

import java.io.Serializable;

public class Peixe implements Serializable {
    private static final int PESO_MAXIMO = 50;
    private double peso;

    public Peixe() {
    }

    public Peixe(double peso) {
        this.peso = peso;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public int getPesoMaximo() {
        return PESO_MAXIMO;
    }

    public boolean isAcimaDoPermitido() {
        if (peso > PESO_MAXIMO) return true;
        return false;
    }
}
