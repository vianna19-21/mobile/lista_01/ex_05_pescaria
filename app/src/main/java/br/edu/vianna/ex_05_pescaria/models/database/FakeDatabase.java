package br.edu.vianna.ex_05_pescaria.models.database;

import br.edu.vianna.ex_05_pescaria.models.Peixe;
import br.edu.vianna.ex_05_pescaria.models.Pescaria;

public class FakeDatabase {
    public static Pescaria pescaria = new Pescaria();

    public static void save(Peixe p) {
        pescaria.addPeixe(p);
    }
}
